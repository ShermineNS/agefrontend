import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

class Age {
}

@Injectable({
  providedIn: 'root'

})
export class AgeService {
  private baseUrl = 'http://localhost:8080/';

  constructor(private http: HttpClient) {
  }

  createAge(age: Age) {
    return this.http.post(`${this.baseUrl}age/add`, age);
  }
}
