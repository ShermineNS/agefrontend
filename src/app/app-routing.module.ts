import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AgeCreateuserComponent} from './age-createuser/age-createuser.component';
import {AgeViewuserComponent} from './age-viewuser/age-viewuser.component';

const routes: Routes = [
{
  path: 'createuser',
    component: AgeCreateuserComponent

},
  {
    path: 'viewuser',
    component:   AgeViewuserComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

