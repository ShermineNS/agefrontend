import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeViewuserComponent } from './age-viewuser.component';

describe('AgeViewuserComponent', () => {
  let component: AgeViewuserComponent;
  let fixture: ComponentFixture<AgeViewuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgeViewuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeViewuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
