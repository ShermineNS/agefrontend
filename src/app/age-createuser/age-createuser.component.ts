import { Component, OnInit } from '@angular/core';
import {AgeService} from '../../AgeService';

export class Age {
  submitted: any;
  firstName: string;
  lastName: string;
  dob: Date;
  presentAge: number;
}

@Component({
  selector: 'app-age-createuser',
  templateUrl: './age-createuser.component.html',
  styleUrls: ['./age-createuser.component.css']
})

export class AgeCreateuserComponent implements OnInit {
  dob: Date;
  presentAge: number;
  age: Age = new Age();

  constructor(private ageservice: AgeService) {
  }

  submitted = false;

  ngOnInit() {
    this.submitted = false;
  }

  CalculateAge() {
    console.log('for checking', this.age.dob);
    if (this.age.dob) {
      const bdate = new Date(this.age.dob);
      const timeDiff = Math.abs(Date.now() - bdate.getTime());
      this.age.presentAge = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);
    }
  }

  saveAge() {
    console.log('age', this.age);
    this.ageservice.createAge(this.age).subscribe(data => {
      console.log('data', data);
    });
  }

}

