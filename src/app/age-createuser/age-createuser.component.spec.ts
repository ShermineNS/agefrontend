import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeCreateuserComponent } from './age-createuser.component';

describe('AgeCreateuserComponent', () => {
  let component: AgeCreateuserComponent;
  let fixture: ComponentFixture<AgeCreateuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgeCreateuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeCreateuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
